#pragma once
#include <SFML\Graphics.hpp>

class WindowScreen;

class ScreenManager
{
	private:
		WindowScreen * currentScreen;
		WindowScreen * nextScreen;
		bool change;
		bool windowDrag;
		sf::Vector2i grabbedOffset;

	private:
		sf::RectangleShape titleBar;
		sf::RectangleShape border;
		sf::Sprite logo;
		sf::Sprite close;
		sf::Text version;

	private:
		sf::Texture logoTexture;
		sf::Texture closeTexture;
		sf::Font font;

	public:
		ScreenManager ( );
		~ScreenManager ( );

	public:
		void LoadScene ( WindowScreen * screen );

		void Event ( sf::Event event, sf::RenderWindow & window );
		void Update ( sf::RenderWindow & window );
		void Render ( sf::RenderWindow & window );
};