#pragma once
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

#include <Windows.h>
#include <ShlObj.h>
#include <urlmon.h>

class DownloadProgress : public IBindStatusCallback
{
	private:
		unsigned long ulProgress;
		unsigned long ulProgressMax;

	public:
		float GetProgress ( );

		unsigned long GetCurrentProgress ( );
		unsigned long GetProgressMax ( );

	public: // Copied from some other source because i was lazy
		HRESULT __stdcall QueryInterface ( const IID &, void ** )
		{
			return E_NOINTERFACE;
		}

		ULONG STDMETHODCALLTYPE AddRef ( void )
		{
			return 1;
		}

		ULONG STDMETHODCALLTYPE Release ( void )
		{
			return 1;
		}

		HRESULT STDMETHODCALLTYPE OnStartBinding ( DWORD dwReserved, IBinding *pib )
		{
			return E_NOTIMPL;
		}

		virtual HRESULT STDMETHODCALLTYPE GetPriority ( LONG *pnPriority )
		{
			return E_NOTIMPL;
		}

		virtual HRESULT STDMETHODCALLTYPE OnLowResource ( DWORD reserved )
		{
			return S_OK;
		}

		virtual HRESULT STDMETHODCALLTYPE OnStopBinding ( HRESULT hresult, LPCWSTR szError )
		{
			return E_NOTIMPL;
		}

		virtual HRESULT STDMETHODCALLTYPE GetBindInfo ( DWORD *grfBINDF, BINDINFO *pbindinfo )
		{
			return E_NOTIMPL;
		}

		virtual HRESULT STDMETHODCALLTYPE OnDataAvailable ( DWORD grfBSCF, DWORD dwSize, FORMATETC *pformatetc, STGMEDIUM *pstgmed )
		{
			return E_NOTIMPL;
		}

		virtual HRESULT STDMETHODCALLTYPE OnObjectAvailable ( REFIID riid, IUnknown *punk )
		{
			return E_NOTIMPL;
		}
	// End of copy

	public:
		virtual HRESULT WINAPI OnProgress 
		(
			unsigned long ulProgress,
			unsigned long ulProgressMax,
			unsigned long ulStatusCode,
			LPCWSTR szStatusText 
		);
};

class GameInstallation
{
	private:
		static GameInstallation * instance;
		static std::string checksum;
		static std::string cdn;

	public:
		static GameInstallation * GetInstance ( )
		{
			if ( instance == nullptr )
			{
				instance = new GameInstallation ( );
			}

			return instance;
		}

	public:
		GameInstallation ( );
		~GameInstallation ( );

	public:
		std::string getGamePath ( );
		void prepareInstallation ( );
};