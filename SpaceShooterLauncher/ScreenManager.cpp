#include "ScreenManager.hpp"
#include "WindowScreen.hpp"
#include "LauncherInstallation.hpp"

ScreenManager::ScreenManager ( )
{
	this->currentScreen = nullptr;
	this->nextScreen = nullptr;

	this->change = false;
	this->windowDrag = false;

	this->font.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\font.ttf" );

	this->titleBar = sf::RectangleShape ( sf::Vector2f ( 800, 100 ) );
	this->titleBar.setFillColor ( sf::Color::Black );

	this->border = sf::RectangleShape ( sf::Vector2f ( 780, 580 ) );
	this->border.setOutlineThickness ( 10 );
	this->border.setOutlineColor ( sf::Color::Green );
	this->border.setPosition ( 10, 10 );
	this->border.setFillColor ( sf::Color ( 0, 0, 0, 0 ) );

	this->logoTexture.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\logo.png" );
	this->logo.setTexture ( this->logoTexture );
	this->logo.setPosition ( 20, 20 );
	this->logo.setScale ( sf::Vector2f ( 0.3f, 0.3f ) );

	this->closeTexture.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\close.png" );
	this->close.setTexture ( this->closeTexture );
	this->close.setScale ( sf::Vector2f ( 0.5f, 0.5f ) );
	this->close.setPosition ( 724, 44 );

	this->version.setFont ( this->font );
	this->version.setFillColor ( sf::Color::White );
	this->version.setPosition ( 40, 550 );
	this->version.setCharacterSize ( 13 );
	this->version.setString ( "version: " + LauncherInstallation::checksum );
}

ScreenManager::~ScreenManager ( )
{
	if ( this->currentScreen != nullptr ) delete this->currentScreen;
	if ( this->nextScreen != nullptr ) delete this->nextScreen;
}

void ScreenManager::LoadScene ( WindowScreen * screen )
{
	// unloading moved to ScreenManager::Update to avoid null pointer
	this->change = true;
	this->nextScreen = screen;
}

void ScreenManager::Event ( sf::Event event, sf::RenderWindow & window )
{
	sf::Vector2i mousePosition = sf::Mouse::getPosition ( window );

	if ( event.type == sf::Event::MouseButtonPressed && this->titleBar.getGlobalBounds ( ).contains ( mousePosition.x, mousePosition.y ) )
	{
		if ( event.mouseButton.button == sf::Mouse::Button::Left )
		{
			if ( !this->close.getGlobalBounds ( ).contains ( mousePosition.x, mousePosition.y ) )
			{
				this->windowDrag = true;
				this->grabbedOffset = window.getPosition ( ) - sf::Mouse::getPosition ( );
			}
		}
	}
	else if ( event.type == sf::Event::MouseButtonReleased )
	{
		if ( event.mouseButton.button == sf::Mouse::Button::Left )
		{
			if ( this->close.getGlobalBounds ( ).contains ( mousePosition.x, mousePosition.y ) )
			{
				UINT r = MessageBox
				(
					NULL,
					"Are you sure that you want to exit launcher?",
					"SpaceShooter Launcher",
					MB_YESNO | MB_ICONEXCLAMATION
				);

				if ( r == IDYES )
				{
					window.close ( );
				}
			}

			this->windowDrag = false;
		}
	}
	else if ( event.type == sf::Event::MouseMoved && this->windowDrag )
	{
		sf::Vector2i newPosition = sf::Vector2i
		(
			sf::Mouse::getPosition ( ).x + grabbedOffset.x,
			sf::Mouse::getPosition ( ).y + grabbedOffset.y
		);

		window.setPosition ( newPosition );
	}

	if ( this->currentScreen != nullptr )
	{
		this->currentScreen->Event ( event );
	}
}

void ScreenManager::Update ( sf::RenderWindow & window )
{
	if ( this->change )
	{
		if ( this->currentScreen != nullptr )
		{
			delete this->currentScreen;
		}

		this->currentScreen = this->nextScreen;
		this->change = false;
	}

	if ( this->currentScreen != nullptr )
	{
		this->currentScreen->Update ( window );
	}
}

void ScreenManager::Render ( sf::RenderWindow & window )
{
	if ( this->currentScreen != nullptr )
	{
		this->currentScreen->Render ( window );
	}

	window.draw ( this->titleBar );
	window.draw ( this->border );
	window.draw ( this->logo );
	window.draw ( this->close );
	window.draw ( this->version );
}
