#pragma once
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

#include <Windows.h>
#include <ShlObj.h>
#include <urlmon.h>

class LauncherInstallation
{
	private:
		static LauncherInstallation * instance;

	public:
		static std::string checksum;
		static std::string cdn;

	private:
		bool cdn_ready = false;

	public:
		static LauncherInstallation * GetInstance ( )
		{
			if ( instance == nullptr )
				instance = new LauncherInstallation ( );

			return instance;
		}

	public:
		LauncherInstallation ( );
		~LauncherInstallation ( );

	public:
		bool isLauncherInstalled ( );
		bool updateAvailable ( );

		void installLauncher ( );
		void prepareInstallation ( );

	private:
		void downloadLauncher ( );

	public:
		std::string getLauncherPath ( );
};