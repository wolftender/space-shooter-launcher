#include <iostream>
#include <cstdlib>
#include <fstream>

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#include "LauncherInstallation.hpp"
#include "ScreenManager.hpp"
#include "MenuScreen.hpp"

#include "resource.h"

ScreenManager * screenManager;

void InitializeLauncher ( bool updateCheck );

int main ( int argc, char ** argv )
{
	bool noUpdateCheck = false;
	bool showConsole = false;

	for ( int i = 0; i < argc; ++i )
	{
		if ( strcmp ( argv[i], "noupdate" ) == 0 )
			noUpdateCheck = true;

		if ( strcmp ( argv[i], "showconsole" ) == 0 )
			showConsole = true;
	}

	if ( !showConsole )
		FreeConsole ( );

	if ( LauncherInstallation::GetInstance ( )->isLauncherInstalled ( ) )
	{
		if ( !noUpdateCheck )
		{
			if ( LauncherInstallation::GetInstance ( )->updateAvailable ( ) )
			{
				UINT r = MessageBox
				(
					NULL,
					"SpaceShooter launcher update is available! Install now?",
					"SpaceShooter Launcher",
					MB_YESNO | MB_ICONEXCLAMATION
				);

				if ( r == IDYES )
				{
					LauncherInstallation::GetInstance ( )->installLauncher ( );
				}
			}
		}
		else
		{
			std::cout << "Launcher started with noupdate switch. No launcher updates will be downloaded or installed." << std::endl;
		}

		InitializeLauncher ( !noUpdateCheck );
	}
	else
	{
		UINT r = MessageBox 
		( 
			NULL, 
			"SpaceShooter launcher installation file was not detected on this PC. Install now?", 
			"SpaceShooter Launcher", 
			MB_YESNO | MB_ICONEXCLAMATION
		);

		if ( r == IDYES )
		{
			LauncherInstallation::GetInstance ( )->installLauncher ( );
			if ( !LauncherInstallation::GetInstance ( )->isLauncherInstalled ( ) ) return 0;

			InitializeLauncher ( !noUpdateCheck );
		}
	}

	return 0;
}

void InitializeLauncher ( bool updateCheck )
{
	HRSRC hIconInfo = FindResource ( GetModuleHandle ( NULL ), MAKEINTRESOURCE ( IDB_PNG1 ), "PNG" );
	HGLOBAL hLoadedResource = LoadResource ( GetModuleHandle ( NULL ), hIconInfo );

	LPVOID hIconResource = LockResource ( hLoadedResource );
	size_t nIconSize = SizeofResource ( GetModuleHandle ( NULL ), hIconInfo );

	sf::Image iconTexture;
	iconTexture.loadFromMemory ( hIconResource, nIconSize );

	screenManager = new ScreenManager ( );

	sf::RenderWindow window ( sf::VideoMode ( 800, 600 ), "SpaceShooter Launcher", sf::Style::None );
	window.setIcon ( iconTexture.getSize ( ).x, iconTexture.getSize ( ).y, iconTexture.getPixelsPtr ( ) );
	window.setFramerateLimit ( 30 );

	MenuScreen * screen = new MenuScreen ( screenManager, updateCheck );
	screenManager->LoadScene ( screen );

	while ( window.isOpen ( ) )
	{
		sf::Event e;

		while ( window.pollEvent ( e ) )
		{
			if ( e.type == sf::Event::Closed )
				window.close ( );

			screenManager->Event ( e, window );
		}

		window.clear ( sf::Color ( 1.0f, 1.0f, 1.0f ) );

		screenManager->Update ( window );
		screenManager->Render ( window );

		window.display ( );
	}
}