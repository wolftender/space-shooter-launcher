#include "MenuScreen.hpp"
#include "GameInstallation.hpp"
#include "LauncherInstallation.hpp"
#include "CDNManager.hpp"
#include "lib/json.hpp"
#include "ScreenManager.hpp"

#include "lib/libmarc.hpp"

#include <Wininet.h>
#include <math.h>

#pragma comment ( lib, "wininet.lib" )

using namespace nlohmann;

MenuScreen::MenuScreen ( ScreenManager * manager, bool updateCheck )
	: WindowScreen ( manager )
{
	this->isGameInstalled = false;
	this->isInstallationActive = false;
	this->isDownloadPreparing = false;

	this->progressBarFill = 100;

	this->versionId = "n/a";
	this->versionHash = "n/a";
	this->binaryPath = GameInstallation::GetInstance ( )->getGamePath ( ) + "\\SpaceShooter.exe";

	this->gameIconTexture.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\invader.png" );
	this->font.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\font.ttf" );
	this->backgroundTexture.loadFromFile ( LauncherInstallation::GetInstance ( )->getLauncherPath ( ) + "\\assets\\background.png" );

	this->background.setTexture ( this->backgroundTexture );
	this->background.setScale ( 2.0f, 2.0f );

	this->gameNotInstalledText = sf::Text ( "SpaceShooter is not installed!", this->font, 20 );
	this->gameNotInstalledText.setPosition ( 180, 200 );

	this->gameIcon.setTexture ( this->gameIconTexture );
	this->gameIcon.setPosition ( 550, 100 );
	this->gameIcon.setScale ( 1.5f, 1.5f );

	this->startButtonRect = sf::RectangleShape ( sf::Vector2f ( 150, 50 ) );
	this->startButtonRect.setFillColor ( sf::Color::Green );
	this->startButtonRect.setPosition ( 570, 470 );

	this->startButtonText = sf::Text ( "undefined", this->font, 16 );
	this->startButtonText.setPosition ( 615, 485 );
	this->startButtonText.setFillColor ( sf::Color::Black );

	this->progressBarBackground = sf::RectangleShape ( sf::Vector2f ( 450, 50 ) );
	this->progressBarBackground.setPosition ( 100, 470 );
	this->progressBarBackground.setFillColor ( sf::Color ( 51, 153, 51 ) );

	this->progressBarForeground = sf::RectangleShape ( sf::Vector2f ( 442, 42 ) );
	this->progressBarForeground.setPosition ( 104, 474 );
	this->progressBarForeground.setFillColor ( sf::Color::Green );

	std::fstream stream ( GameInstallation::GetInstance ( )->getGamePath ( ) + "\\version.json" );

	if ( stream.good ( ) )
	{
		try
		{
			json j;
			j << stream;

			this->versionId = j.at ( "GameVersion" ).at ( "id" ).get <std::string> ( );
			this->versionHash = j.at ( "GameVersion" ).at ( "hash" ).get <std::string> ( );
			this->binaryPath = GameInstallation::GetInstance ( )->getGamePath ( ) + "\\" + j.at ( "GameVersion" ).at ( "exe" ).get <std::string> ( );

			json cl = j.at ( "GameVersion" ).at ( "changelog" );

			std::string changelogString = "";

			for ( auto it = cl.begin ( ); it != cl.end ( ); ++it )
			{
				changelogString += " *" + it->get < std::string > ( ) + "\n";
			}

			sf::Vector2f basePosition = sf::Vector2f ( 80, 150 );

			// Now we put those in texts
			this->versionIdText = sf::Text ( "Space Shooter v" + this->versionId, this->font, 20 );
			this->versionIdText.setPosition ( basePosition.x, basePosition.y );

			this->versionHashText = sf::Text ( "(" + this->versionHash + ")", this->font, 16 );
			this->versionHashText.setPosition ( basePosition.x + 20, basePosition.y + 40 );

			this->binaryPathText = sf::Text ( "Path: " + this->binaryPath, this->font, 16 );
			this->binaryPathText.setPosition ( basePosition.x + 20, basePosition.y + 80 );

			this->changelogText = sf::Text ( "Changelog: \n" + changelogString, this->font, 16 );
			this->changelogText.setPosition ( basePosition.x + 20, basePosition.y + 80 );

			this->isGameInstalled = true;
		}
		catch ( int e )
		{
			std::cout << "Invalid version.json formatting!" << std::endl;
			this->isGameInstalled = false;
		}
	}
	else
	{
		std::cout << "version.json not found!" << std::endl;
		this->isGameInstalled = false;
	}

	if ( updateCheck )
	{
		checkUpdates ( );
	}
}

MenuScreen::~MenuScreen ( ) { }

void MenuScreen::checkUpdates ( )
{
	if ( !this->isGameInstalled ) return;

	std::string cdn = CDNManager::GetInstance ( )->getCDN ( );

	std::fstream stream ( cdn );

	std::string currentVersionHash = this->versionHash;
	std::string newestVersionHash = "";

	if ( stream.is_open ( ) )
	{
		json j;
		j << stream;

		newestVersionHash = j.at ( "cdn" ).at ( "gameid" ).get <std::string> ( );
	}
	else
	{
		MessageBox ( NULL, "Failed to download launcher! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
	}

	stream.close ( );

	if ( currentVersionHash != newestVersionHash )
	{
		if ( MessageBox ( NULL, "A new game version is available for download. Would you like to download it?", "SpaceShooter Launcher", MB_YESNO | MB_ICONASTERISK ) == IDYES )
			this->install ( );
	}
}

void MenuScreen::install ( )
{
	// Prepare the installation
	GameInstallation::GetInstance ( )->prepareInstallation ( );

	this->isInstallationActive = true;
	this->downloadProgress = new DownloadProgress ( );

	dlThread = new sf::Thread ( &MenuScreen::download, this );
	dlThread->launch ( );
}

void MenuScreen::run ( )
{
	std::cout << "Running the game!" << std::endl;

	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	ZeroMemory ( &pi, sizeof ( pi ) );
	ZeroMemory ( &si, sizeof ( si ) );

	CreateProcess 
	(
		this->binaryPath.c_str ( ),
		NULL,
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS,
		NULL,
		GameInstallation::GetInstance ( )->getGamePath ( ).c_str ( ),
		&si,
		&pi
	);

	window->close ( );
}

void MenuScreen::download ( )
{
	std::cout << "Download Started!" << std::endl;

	std::string path = CDNManager::GetInstance ( )->getCDN ( );
	std::fstream stream ( path );

	if ( stream.is_open ( ) )
	{
		json j;
		j << stream;

		std::string dl = j.at ( "cdn" ).at ( "gamedl" ).get <std::string> ( );
		std::string dlpath = GameInstallation::GetInstance ( )->getGamePath ( ) + "\\game.pak";

		std::cout << "Obtained Download URL: " << dl << std::endl;

		HRESULT hr;

		DeleteUrlCacheEntry ( dl.c_str ( ) );

		if ( FAILED ( hr = URLDownloadToFile ( NULL, dl.c_str ( ), dlpath.c_str ( ), 0, this->downloadProgress ) ) )
		{
			MessageBox ( NULL, "Failed to download archive! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
		}

		std::cout << "Downloaded launcher installation archive!" << std::endl;

		decompress ( dlpath, GameInstallation::GetInstance ( )->getGamePath ( ) + "\\" );

		std::cout << "Decompressing succeeded! Game is ready!" << std::endl;

		this->GetManager ( )->LoadScene ( new MenuScreen ( this->GetManager ( ), false ) );
	}
	else
	{
		MessageBox ( NULL, "Failed to download launcher! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
		this->GetManager ( )->LoadScene ( new MenuScreen ( this->GetManager ( ), false ) );
	}
}

void MenuScreen::Event ( sf::Event event )
{
	if ( event.type == sf::Event::MouseButtonReleased )
	{
		if ( this->startButtonRect.getGlobalBounds ( ).contains ( mousePosition.x, mousePosition.y ) )
		{
			if ( !this->isInstallationActive )
			{
				if ( !this->isGameInstalled )
				{
					this->install ( );
				}
				else
				{
					this->run ( );
				}
			}
		}
	}
}

void MenuScreen::Update ( sf::RenderWindow & window )
{
	this->window = &window;
	mousePosition = sf::Mouse::getPosition ( window );

	sf::Vector2f progressScale = sf::Vector2f ( this->progressBarFill / 100.0f, 1.0f );
	this->progressBarForeground.setScale ( progressScale );

	if ( this->isInstallationActive )
	{
		this->startButtonText.setPosition ( 625, 485 );
		double p = ( double ) this->downloadProgress->GetCurrentProgress ( );
		double m = ( double ) this->downloadProgress->GetProgressMax ( );
		int percentage = floor ( p / m * 100.0f );

		if ( m == 0 )
		{
			this->startButtonText.setString ( "0%" );
			this->progressBarFill = 0;
		}
		else
		{
			this->startButtonText.setString ( std::to_string ( percentage ) + "%" );
			this->progressBarFill = percentage;
		}
	}
	else
	{
		if ( this->isGameInstalled )
		{
			this->startButtonText.setPosition ( 615, 485 );
			this->startButtonText.setString ( "Play" );
		}
		else
		{
			this->startButtonText.setPosition ( 600, 485 );
			this->startButtonText.setString ( "Install" );
		}
	}
}

void MenuScreen::Render ( sf::RenderWindow & window )
{
	window.draw ( this->background );

	// Two different looks based on game presence
	if ( this->isGameInstalled )
	{
		window.draw ( this->versionIdText );
		window.draw ( this->versionHashText );
		window.draw ( this->changelogText );
		window.draw ( this->gameIcon );
	}
	else
	{
		window.draw ( this->gameNotInstalledText );
	}

	window.draw ( this->progressBarBackground );
	window.draw ( this->progressBarForeground );

	window.draw ( this->startButtonRect );
	window.draw ( this->startButtonText );
}