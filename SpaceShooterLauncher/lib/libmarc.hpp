#pragma once
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <ios>

#include <Windows.h>
#include <ShlObj.h>

struct FileData
{
	std::string filename;
	int size;
};

void compress ( std::string data, std::string outfile );
void decompress ( std::string data, std::string destination );