#include "libmarc.hpp"

int get_file_size ( std::string file )
{
	std::fstream stream ( "files/" + file );

	if ( !stream.is_open ( ) )
		return -1; // file does not exist

	stream.seekg ( 0, stream.end );
	int size = stream.tellg ( );
	stream.close ( );

	return size;
}

std::vector < FileData > get_file_data ( std::string data )
{
	std::fstream stream ( data );
	std::vector < FileData > fl;

	if ( stream.good ( ) )
	{
		std::string in;

		while ( std::getline ( stream, in ) )
		{
			FileData fd;
			fd.filename = in;
			fd.size = get_file_size ( in );

			fl.push_back ( fd );
		}
	}

	stream.close ( );
	return fl;
}

void create_path ( std::string path )
{
	std::string fragment = "";
	std::vector < std::string > fragments;

	for ( std::string::iterator it = path.begin ( ); it != path.end ( ); ++it )
	{
		if ( ( *it ) == '/' || ( *it ) == '\\' )
		{
			fragments.push_back ( fragment );
			fragment = "";
		}
		else
		{
			fragment += ( *it );
		}
	}

	std::string dir = "";

	for ( auto it = fragments.begin ( ); it != fragments.end ( ); ++it )
		dir += *it + "\\";

	/*char curr[256];
	dir = curr + dir;*/

	// std::cout << dir << std::endl;

	SHCreateDirectoryEx ( NULL, dir.c_str ( ), NULL );
}

void compress ( std::string data, std::string outfile )
{
	std::vector < FileData > fd = get_file_data ( data );
	std::ofstream out ( outfile, std::ios::binary );

	for ( std::vector < FileData >::iterator it = fd.begin ( ); it != fd.end ( ); ++it )
	{
		if ( it->size > 0 )
		{
			std::string file = it->filename;
			file += ( ( char ) 0 );

			std::ifstream stream ( "files/" + it->filename, std::ios::binary );
			out << file;
			out.write ( reinterpret_cast < const char * > ( &it->size ), sizeof ( int ) );
			out << stream.rdbuf ( );

			stream.close ( );

			std::cout << "Added " << it->filename << " to archive!" << std::endl;
		}
	}

	out.close ( );
}

void decompress ( std::string data, std::string destination )
{
	std::ifstream in ( data, std::ios::binary );

	if ( in.good ( ) )
	{
		bool readingFile = false;
		int fileBytes = 0;

		std::string filename;
		std::string size_str;
		size_t size = 0;

		char c;
		in.read ( &c, 1 );

		while ( !in.eof ( ) )
		{
			while ( c != 0 ) // read name
			{
				filename += c;
				in.read ( &c, 1 );
			}

			in.read ( reinterpret_cast < char * > ( &size ), sizeof ( int ) );
			std::cout << "NAME: " << filename << " LENGTH: " << size << std::endl;

			char * buffer = new char[size + 1];
			buffer[size] = '\0';
			in.read ( buffer, size );

			create_path ( destination + "/" + filename );
			// SHCreateDirectoryEx ( NULL, destination.c_str ( ), NULL );
			std::ofstream out ( destination + "/" + filename, std::ios::binary );

			out.write ( buffer, size );
			out.close ( );

			filename = "";
			size_str = "";

			in.read ( &c, 1 );
		}
	}
	else
	{
		std::cout << "Error when reading file " << data << std::endl;
	}
}