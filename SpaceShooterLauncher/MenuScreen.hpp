#pragma once
#include "WindowScreen.hpp"

class DownloadProgress;

class MenuScreen : public WindowScreen
{
	private:
		bool isGameInstalled;
		bool isInstallationActive;
		bool isDownloadPreparing;

		int progressBarFill;

		sf::Vector2i mousePosition;

		DownloadProgress * downloadProgress;
		sf::Thread * dlThread;

		std::string binaryPath;
		std::string versionId;
		std::string versionHash;

		std::vector < std::string > changelog;

		sf::RenderWindow * window;

	private:
		// Renderables
		sf::Text versionIdText;
		sf::Text versionHashText;
		sf::Text binaryPathText;
		sf::Text changelogText;

		sf::Text gameNotInstalledText;

		sf::Sprite gameIcon;
		sf::Sprite installGameButton;
		sf::Sprite background;

		sf::Text startButtonText;
		sf::RectangleShape startButtonRect;
		sf::RectangleShape progressBarBackground;
		sf::RectangleShape progressBarForeground;

	private:
		// Resources
		sf::Font font;
		sf::Texture gameIconTexture;
		sf::Texture installGameTexture;
		sf::Texture backgroundTexture;

	public:
		MenuScreen ( ScreenManager * manager, bool updateCheck );
		~MenuScreen ( );

	private:
		void checkUpdates ( );
		void install ( );
		void run ( );
		void download ( );

	public:
		virtual void Event ( sf::Event event ) override;
		virtual void Update ( sf::RenderWindow & window ) override;
		virtual void Render ( sf::RenderWindow & window ) override;
};