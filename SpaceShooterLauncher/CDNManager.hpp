#pragma once
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

#include <Windows.h>
#include <ShlObj.h>
#include <urlmon.h>

class CDNManager
{
	private:
		static CDNManager * instance;

		const std::string cdn = std::string ( "https://www.dl.dropboxusercontent.com/s/p8k8bau3xoh1uu4/cdn.dat" );

	public:
		static CDNManager * GetInstance ( )
		{
			if ( instance == nullptr )
			{
				instance = new CDNManager ( );
			}

			return instance;
		}

	private:
		bool cdn_ready = false;

	public:
		CDNManager ( );
		~CDNManager ( );

		std::string getCDN ( );

	private:
		std::string getCDNPath ( );
};