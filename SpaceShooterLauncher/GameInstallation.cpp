#include "GameInstallation.hpp"

GameInstallation * GameInstallation::instance = nullptr;

GameInstallation::GameInstallation ( ) { }

GameInstallation::~GameInstallation ( )
{
	if ( instance != nullptr )
	{
		delete instance;
	}
}

std::string GameInstallation::getGamePath ( )
{
	LPSTR lpAppDataPath = new TCHAR[256];
	SHGetFolderPath ( NULL, CSIDL_MYDOCUMENTS, NULL, NULL, lpAppDataPath );

	std::string strAppInstallPath = lpAppDataPath;
	strAppInstallPath += "\\Games\\SpaceShooter\\Game";

	return strAppInstallPath;
}

void GameInstallation::prepareInstallation ( )
{
	std::string strAppInstallPath = this->getGamePath ( );
	SHCreateDirectoryEx ( NULL, strAppInstallPath.c_str ( ), NULL );
}

float DownloadProgress::GetProgress ( )
{
	if ( this->ulProgressMax == 0 )
	{
		return 0.0f;
	}
	else
	{
		return this->ulProgress / this->ulProgressMax;
	}
}

unsigned long DownloadProgress::GetCurrentProgress ( )
{
	return this->ulProgress;
}

unsigned long DownloadProgress::GetProgressMax ( )
{
	return this->ulProgressMax;
}

HRESULT DownloadProgress::OnProgress ( unsigned long ulProgress, unsigned long ulProgressMax, unsigned long ulStatusCode, LPCWSTR szStatusText )
{
	this->ulProgress = ulProgress;
	this->ulProgressMax = ulProgressMax;
 
	return S_OK;
}
