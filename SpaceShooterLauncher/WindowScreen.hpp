#pragma once
#include "SFML\Window.hpp"
#include "SFML\System.hpp"
#include "SFML\Graphics.hpp"

class ScreenManager;

class WindowScreen
{
	private:
		ScreenManager * manager;

	protected:
		ScreenManager * GetManager ( );

	public:
		WindowScreen ( ScreenManager * manager );
		~WindowScreen ( );

	public:
		virtual void Event ( sf::Event event ) { }
		virtual void Update ( sf::RenderWindow & window ) { }
		virtual void Render ( sf::RenderWindow & window ) { }
};