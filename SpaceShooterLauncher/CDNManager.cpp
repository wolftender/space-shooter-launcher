#include "CDNManager.hpp"

CDNManager * CDNManager::instance = nullptr;

CDNManager::CDNManager ( ) { }

CDNManager::~CDNManager ( )
{
	if ( instance != nullptr )
	{
		delete instance;
	}
}

std::string CDNManager::getCDN ( )
{
	std::string path = this->getCDNPath ( ) + "\\cdn.dat";

	if ( !this->cdn_ready )
	{
		std::cout << "Downloading CDN info file..." << std::endl;

		HRESULT hr;

		if ( FAILED ( hr = URLDownloadToFile ( NULL, cdn.c_str ( ), path.c_str ( ), 0, NULL ) ) )
		{
			MessageBox ( NULL, "Failed to connect to CDN! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
		}

		std::cout << "Downloaded CDN info file!" << std::endl;

		cdn_ready = true;
	}

	return path;
}

std::string CDNManager::getCDNPath ( )
{
	LPSTR lpAppDataPath = new TCHAR[256];
	SHGetFolderPath ( NULL, CSIDL_MYDOCUMENTS, NULL, NULL, lpAppDataPath );

	std::string strAppInstallPath = lpAppDataPath;
	strAppInstallPath += "\\Games\\SpaceShooter";

	SHCreateDirectoryEx ( NULL, strAppInstallPath.c_str ( ), NULL );

	return strAppInstallPath;
}
