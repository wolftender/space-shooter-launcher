#include "LauncherInstallation.hpp"
#include "GameInstallation.hpp"
#include "CDNManager.hpp"

#include <fstream>
#include "lib/json.hpp"
#include "lib/libmarc.hpp"

using namespace nlohmann;

LauncherInstallation * LauncherInstallation::instance = nullptr;
std::string LauncherInstallation::checksum = std::string ( "bc909fc0236307245f05d024de462d96" ); // "plazmidy" md5

#pragma comment ( lib, "urlmon.lib" )

LauncherInstallation::LauncherInstallation ( ) { }

LauncherInstallation::~LauncherInstallation ( )
{
	if ( instance != nullptr )
	{
		delete instance;
	}
}

bool LauncherInstallation::isLauncherInstalled ( )
{
	std::string launcherPath = this->getLauncherPath ( );
	launcherPath += "\\version.dat";

	std::fstream stream ( launcherPath );

	return stream.good ( );
}

void LauncherInstallation::installLauncher ( )
{
	this->prepareInstallation ( );
	this->downloadLauncher ( );
}

void LauncherInstallation::prepareInstallation ( )
{
	std::string strAppLauncherPath = this->getLauncherPath ( );
	SHCreateDirectoryEx ( NULL, strAppLauncherPath.c_str ( ), NULL );
}

bool LauncherInstallation::updateAvailable ( )
{
	std::string path = CDNManager::GetInstance ( )->getCDN ( );
	std::fstream stream ( path );

	if ( stream.is_open ( ) )
	{
		json j;
		j << stream;

		std::string id = j.at ( "cdn" ).at ( "launcherid" ).get <std::string> ( );
		
		return ( id != this->checksum );
	}
	else
	{
		std::cout << "Could not check for launcher update!" << std::endl;
		return false;
	}

	return true;
}

void LauncherInstallation::downloadLauncher (  )
{
	std::string path = CDNManager::GetInstance ( )->getCDN ( );
	std::fstream stream ( path );

	if ( stream.is_open ( ) )
	{
		json j;
		j << stream;

		std::string dl = j.at ( "cdn" ).at ( "launcherdl" ).get <std::string> ( );
		std::string dlpath = this->getLauncherPath ( ) + "\\launcher.pak";

		HRESULT hr;

		if ( FAILED ( hr = URLDownloadToFile ( NULL, dl.c_str ( ), dlpath.c_str ( ), 0, NULL ) ) )
		{
			MessageBox ( NULL, "Failed to download archive! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
		}

		std::cout << "Downloaded launcher installation archive!" << std::endl;
		decompress ( dlpath, this->getLauncherPath ( ) + "\\" );
	}
	else
	{
		MessageBox ( NULL, "Failed to download launcher! Please try again later!", "SpaceShooter Launcher", MB_OK | MB_ICONERROR );
	}
}

std::string LauncherInstallation::getLauncherPath ( )
{
	LPSTR lpAppDataPath = new TCHAR [256];
	SHGetFolderPath ( NULL, CSIDL_MYDOCUMENTS, NULL, NULL, lpAppDataPath );

	std::string strAppDataPath = lpAppDataPath;
	strAppDataPath += "\\Games\\SpaceShooter\\Launcher";

	delete lpAppDataPath;
	return strAppDataPath;
}
